# 基于LSTM的文本情感分类任务教程

👉5分钟快速体验AI全流程开发

在此教程中，我们将通过Fork项目MindSpore/LSTM，快速体验AI全流程开发——训练、推理和评估。

# 目录  
[一、基本介绍](#一基本介绍)  

- [任务简介](#1-任务简介)
- [项目地址](#2-项目地址)
- [项目结构](#3-项目结构)

[二、效果展示](#二效果展示)
- [训练](#1-训练)
- [推理](#2-推理)

[三、快速开始](#三快速开始)

- [Fork样例仓](#1-fork样例仓)
- [训练与评估](#2-模型训练)
- [在线推理](#3-在线推理)

[四、问题反馈](#四问题反馈)


## 一、基本介绍

### 1. 任务简介

在这个项目中，我们将基于公开的模型仓库 MindSpore/LSTM 进行文本情感分类模型的训练、在线推理。情感分类是自然语言处理（NLP）中的一项关键任务，它涉及到判断文本（如用户评论或社交媒体帖子）表达的情感倾向是积极的还是消极的。这种技术在商业分析、客户服务和社交媒体监控中有着广泛的应用。

#### a) LSTM简介
在自然语言处理(NLP)等序列数据任务中，循环神经网络（RNN）因其在处理时间序列数据上的优势而广泛使用。然而，标准RNNs的简单细胞结构使得它们在训练过程中容易遭受梯度消失或梯度爆炸的问题，尤其是处理较长序列时。这种问题通常导致网络难以学习并保留序列开始部分的信息。为了解决这一问题，1997年Hochreiter和Schmidhuber提出了长短期记忆网络（LSTM）。LSTM通过引入门控机制有效克服了梯度消失的问题，使得网络能够在更长的序列上维持信息的流动，提高了模型在长序列数据处理上的性能。LSTM单元包括三个主要的门控结构——输入门、遗忘门和输出门，它们共同决定信息如何添加或移除，以此维持或更新细胞状态：

- **遗忘门**：决定哪些信息需要被遗忘，即从细胞状态中去除。这是通过一个Sigmoid层实现的，它输出一个0到1之间的值，0表示完全遗忘，而1表示完全保留。
- **输入门**：决定哪些新信息被存储在细胞状态中。它包括一个Sigmoid层，决定哪些值我们将更新，以及一个Tanh层，创建一个新的候选值向量，将被加到状态中。
- **输出门**：决定下一个隐藏状态的值，隐藏状态包含关于先前输入的信息，用于预测或决定接下来的操作。输出门通过对细胞状态使用Tanh函数（将值正规化到-1到1之间）和由Sigmoid门控制的输出决定。

这些门控机制的引入使得LSTM能够动态地调节信息流，使网络在需要记忆过去信息时变得更加有效和精确。下图为LSTM的细胞结构拆解：

![lstm-model](./pic/lstm-model.png)

#### b) 数据集简介
使用的数据集是情感分类的经典数据集，来自IMDB电影评论。这个数据集广泛用于自然语言处理的研究和应用，特别是在情感分析领域。数据集包含了来自[IMDB网站](https://www.imdb.com/)的大量电影评论，这些评论被标记为正面（Positive）或负面（Negative），用于二分类任务。IMDB数据集提供了25,000条电影评论用于训练，25,000条电影评论用于测试。下面为其样例：

| Review                                                       | Label    |
| ------------------------------------------------------------ | -------- |
| “Quitting” may be as much about exiting a pre-ordained identity as about drug withdrawal. As a rural guy coming to Beijing, class and success must have struck this young artist face on as an appeal to separate from his roots and far surpass his peasant parents’ acting success. Troubles arise, however, when the new man is too new, when it demands too big a departure from family, history, nature, and personal identity. The ensuing splits, and confusion between the imaginary and the real and the dissonance between the ordinary and the heroic are the stuff of a gut check on the one hand or a complete escape from self on the other. | Negative |
| This movie is amazing because the fact that the real people portray themselves and their real life experience and do such a good job it’s like they’re almost living the past over again. Jia Hongsheng plays himself an actor who quit everything except music and drugs struggling with depression and searching for the meaning of life while being angry at everyone especially the people who care for him most. | Positive |


### 2. 项目地址
- 项目仓库： [MindSpore/LSTM](https://xihe.mindspore.cn/projects/MindSpore/LSTM) \- 包含所有项目代码和文档，是进行LSTM文本情感分类模型训练和推理的主要仓库。
- 模型仓库： [MindSpore/LSTM_model](https://xihe.mindspore.cn/models/MindSpore/LSTM_model) \- 存放已训练好的LSTM模型，可用于快速部署和测试。
- 数据集仓库： [drizzlezyk/imdb_dataset](https://xihe.mindspore.cn/datasets/drizzlezyk/imdb_dataset) - 提供用于情感分类训练的IMDB电影评论数据集。

### 3. 项目结构

项目的目录分为两个部分：推理（inference）和训练（train），推理可视化相关的代码放在inference文件夹下，训练相关的代码放在train文件夹下。

```python
 ├── inference    # 推理可视化相关代码目录
 │  ├── app.py    # 推理核心启动文件
 │  └── pip-requirements.txt    # 推理可视化相关依赖文件
 └── train    # 在线训练相关代码目录
   ├── pip-requirements.txt  # 训练代码所需要的package依赖声明文件
   ├── lstm_aim_cust.py  # 自定义Aim训练代码 
   └── train.py       # 神经网络训练代码
```


## 二、效果展示

### 1. 训练

训练阶段是模型开发的核心部分，它直接决定了模型在实际应用中的表现。在本项目中，我们使用了IMDB数据集来训练一个基于LSTM的文本情感分类模型。下图展示了训练过程中的一些关键指标，如损失函数的下降和准确率的提升，这些指标帮助我们监控和优化模型的学习过程。![train_result](./pic/train_result.png)

### 2. 推理

推理阶段是指使用训练好的模型对新的数据进行预测的过程。下面的图像展示了一个推理界面，用户可以在这个界面输入一段文本，模型会评估并返回这段文本的情感倾向（正面或负面）。这个界面基于Gradio库构建，提供了一个直观的方式来演示模型的实时应用效果。在下图的这个例子中，用户输入了一段评价，模型成功地识别出其正面的情感倾向。

 ![inference_result](./pic/inference_result.png)


## 三、快速开始
### 1. Fork样例仓

Forking一个项目仓库是复制一份原始项目到你的账户下的过程，这样你就可以自由地进行修改而不影响原始项目。这是开始自定义项目的第一步。

1. **访问MindSpore官方平台**：打开浏览器，访问[MindSpore大模型平台](https://xihe.mindspore.cn/)。
2. **搜索项目仓库**：在项目搜索栏中输入“MindSpore/LSTM”，找到对应的项目仓库。这个仓库包含了所有你需要的代码和资源来开始文本情感分类模型的训练和推理。
3. **Fork项目**：在项目页面上，找到并点击“**Fork**”按钮。这将会创建项目的一个副本到你的账户下，允许你进行自由的修改和实验。

![search_result](./pic/search_result.png)

### 2. 模型训练

模型训练是机器学习项目中的核心部分，通过训练可以使模型学习到数据中的规律和特征。在MindSpore平台上进行模型训练涉及以下步骤：

#### a) 创建训练实例

1. **选择训练配置**：

- 打开项目界面，点击“**训练**”页签。
- 点击“**创建训练实例**”按钮开始设置你的训练实例。
- 填写训练名称，并选择代码目录和启动文件。这些设置将确定训练使用的脚本和环境。

![train_config](./pic/train_config.png)

2. **输入模型、数据集、输出路径等参数指定：**

- 在表单中指定使用的预训练模型文件存放路径（文件存放在昇思大模型平台的模型模块下）
- 在表单中指定使用的数据集文件存放路径（文件存放在昇思大模型平台的数据集模块下）
- 训练的输出结果统一指定超参数名：output_path，需要在代码的argparse模块声明

![parameters_config](./pic/parameters_config.png)

3. **启动训练**：

- 点击“创建训练”完成设置并开始训练过程。注意，同时只能有一个运行中的训练实例，且每个仓库最多允许五个实例。

#### b) 查看训练进度

1. **训练列表**：

- 要查看所有训练实例，将鼠标放置于“**训练**”栏上，并点击下拉框中的“**训练列表**”。

![train_list](./pic/train_list.png)

2. **训练日志**：

- 点击特定训练名称进入详情页面，这里你可以查看训练日志和其他重要信息。
- 所有输出到超参数output_path的文件都在tar.gz文件中。

![log](./pic/log.png)

#### c) 自定义评估

- 如需进行自定义评估，需在创建实例时激活自定义评估选项，并使用特定的启动文件（如`lstm_aim_cust.py`）。
- 确保评估脚本中包含必要的超参数`aim_repo`，以便正确连接到评估平台。

### 3. 在线推理

在线推理是使用训练好的模型对实时数据进行分类的过程。在这个项目中，推理模块使用了LSTM模型来对文本数据进行情感分类，将文本分类为Positive（正面）或Negative（负面）。

#### a) 启动推理服务

1. **启动界面**：

- 在项目的顶部菜单中找到并选择“**推理**”页签。
- 点击“**启动**”按钮以初始化在线推理服务。

这一过程大约需要两分钟，期间后台将加载训练好的模型并准备推理环境。

![interface](./pic/interface.png)

2. **进行文本推理**：

- 推理服务启动后，界面将显示一个文本输入框。
- 将你想要分析的文本输入到框中，然后提交。
- 系统将处理输入的文本，并显示情感分类结果。

#### b) 推理效果展示

- **示例结果**：以下是一个推理示例，其中模型成功识别了输入文本的情感倾向。

![inf_result](./pic/inf_result.png)

## 四、问题反馈

您如果按照教程在操作过程中出现任何问题，请您随时在我们的官网仓提issue，我们会及时回复您。如果您有任何建议，也可以添加官方助手小猫子（微信号：mindspore0328），我们非常欢迎您的宝贵建议，如被采纳，会收到MindSpore官方精美礼品哦！
